Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'urls#home'

  get '/(:short_url_path)', to: "urls#index"

  resources :urls do
    collection do
      get :home
    end
  end
end
