class UrlsController < ApplicationController

  def index
    url = Url.find_by(short_url_path: params[:short_url_path])
    if url.present?
      redirect_to url.original_url

      url.inc(all_clicks: 1)
    end
  end

  def home
    @urls  = Url.all
  end

  def create
    @url = Url.new(url_params)
    @url.save

    redirect_to root_path
  end

  private

  def url_params
    params.require(:url).permit(:original_url, :short_url, :short_url_path)
  end
end
